package exceptions;

public class OutOfMoneyException extends Exception {
    public OutOfMoneyException(final String name) {
        super(name);
    }
}
