package exceptions;

public class TooLazyException extends Exception {
    public TooLazyException(final String name) {
        super(name);
    }
}