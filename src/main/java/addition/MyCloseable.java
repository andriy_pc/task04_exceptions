package addition;

import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;

public class MyCloseable implements AutoCloseable {
    private static int counter = 0; // to throw exception when counter == 5;
    private String fileName = "D:\\tmp";
    private File file;
    private PrintWriter out;

    public MyCloseable() {
        try {
            file = new File(fileName + counter + ".txt");
            out = new PrintWriter(file);
            out.write(counter);
            out.write(" was created\n");
            ++counter;
        } catch (IOException ioe) {
            ioe.printStackTrace(System.out);
        }
    }

    public void close() throws ForcedException {
        if (counter >= 5) {
            throw new ForcedException("counter >= 5!");
        } else if (out != null && file != null) {
            out.close();
        }
    }
}

class ForcedException extends Exception {
    public ForcedException(final String message) {
        super(message);
    }
}