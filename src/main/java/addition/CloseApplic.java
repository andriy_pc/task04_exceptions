package addition;

import java.util.concurrent.TimeUnit;

public class CloseApplic {
    public static void main(final String[] args) {
        for (int i = 0; i < 7; i++) {
            try (MyCloseable mc = new MyCloseable()) {
                System.out.println("going to sleep");
                System.out.println("mc.counter = " + i);
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (ForcedException fe) {
                System.out.println(fe.getMessage());
                throw new RuntimeException();
            } catch (InterruptedException ie) {
                ie.printStackTrace(System.out);
            }
        }
    }
}
