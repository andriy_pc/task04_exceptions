package mvc;

import java.util.ArrayList;
import java.util.Random;
import exceptions.OutOfMoneyException;
import exceptions.TooLazyException;
import exceptions.NullSleepException;



public class Data {
    private Random rand = new Random(47);
    /*Array is intended to use once created Exceptions,
    * Instead of creating new.*/
    private Exception[] data = new Exception[] {
            new OutOfMoneyException("Fu*k! Out of money again!"),
            new TooLazyException("I'l do it tomo...never"),
            new NullSleepException("Error 4:04")
    };

    ArrayList<Exception> generateRandomly(final int quantity) {
        int index; //uses to add Exception from the array into List
        ArrayList<Exception> result = new ArrayList<Exception>();
        for (int i = 0; i < quantity; ++i) {
            index = rand.nextInt(3);
            switch (index) {
                case 0: {
                    result.add(data[0]); break;
                }
                case 1: {
                    result.add(data[1]);
                    break;
                }
                case 2: {
                    result.add(data[2]);
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return result;
    }
    void generateOutOfMoney() throws OutOfMoneyException {
        OutOfMoneyException oom = (OutOfMoneyException) data[0];
        throw oom;
    }
    void generateTooLazy() throws TooLazyException {
        TooLazyException tl = (TooLazyException) data[1];
        throw tl;
    }
    void generateNullSleep() throws NullSleepException {
        NullSleepException ns = (NullSleepException) data[2];
        throw ns;
    }
}
