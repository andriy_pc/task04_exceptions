package exceptions;

public class NullSleepException extends Exception {
    public NullSleepException(final String name) {
        super(name);
    }
}
