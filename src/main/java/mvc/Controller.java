package mvc;

import java.util.ArrayList;
import java.util.Scanner;

import exceptions.OutOfMoneyException;
import exceptions.TooLazyException;
import exceptions.NullSleepException;

public class Controller {
    private Data data;
    private View view;
    private ArrayList<Exception> dataStorage;
    private Scanner scan = new Scanner(System.in);

    private String mainMenu = "---------------------------\n"
            +
            "Please, select action!\n"
            +
            "0) quit\n"
            +
            "1) generate exceptions randomly\n"
            +
            "2) generate OutOfMoneyException\n"
            +
            "3) generate TooLazyException\n"
            +
            "4) generate NullSleepException\n"
            +
            "---------------------------\n";

    Controller(final Data customData, final View customView) {
        this.data = customData;
        this.view = customView;
        dataStorage = new ArrayList<Exception>();
    }

    public boolean mainLoop() {
        view.displayText(mainMenu);
        int choice = scan.nextInt();
        switch (choice) {
            case 0: {
                return false;
            }
            case 1: {
                System.out.println("Please input quantity");
                generateRandomly(scan.nextInt());
                break;
            }
            case 2: {
                generateOutOfMoney();
                break;
            }
            case 3: {
                generateTooLazy();
                break;
            }
            case 4: {
                generateNullSleep();
                break;
            }
            default : {
                System.out.println("Please, select in range 0-4!");
                break;
            }
        }
        return true;
    }

    private void generateOutOfMoney() {
        try {
            data.generateOutOfMoney();
        } catch (OutOfMoneyException oome) {
            System.out.println(oome.getMessage());
        }
    }

    private void generateTooLazy() {
        try {
            data.generateTooLazy();
        } catch (TooLazyException tle) {
            System.out.println(tle.getMessage());
        }
    }

    private void generateNullSleep() {
        try {
            data.generateNullSleep();
        } catch (NullSleepException nse) {
            System.out.println(nse.getMessage());
        }
    }

    private void generateRandomly(final int quantity) {
        dataStorage = data.generateRandomly(quantity);
        throwRandomly();
    }

    private void throwRandomly() {
        if (!dataStorage.isEmpty()) {
            try {
                throw dataStorage.get(0);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                dataStorage.remove(0);
                throwRandomly();
            }
        }
    }
}

