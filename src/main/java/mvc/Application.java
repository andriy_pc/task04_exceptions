package mvc;

public class Application {
    public static void main(final String[] args) {
        Data data = new Data();
        View view = new View();
        Controller c = new Controller(data, view);
        boolean isActive = true;
        while (isActive) {
            isActive = c.mainLoop();
        }

    }
}
